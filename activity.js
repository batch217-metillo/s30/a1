// Count the total number of fruits on sale
db.fruits.aggregate([
	{$match: {onSale: true}},
	{$count: "fruitsOnSale"}
]);

// Count the total number of fruits with stock more than 20
db.fruits.aggregate([
	{$match: {stock: {$gt: 20}}},
	{$count: "fruitsWithEnoughStocks"}
]);

// Get the average price of fruits on sale per supplier
db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", averagePrice: {$avg: "$price"}}}
]);

// Get the highest price of a fruit per supplier
db.fruits.aggregate([
	{$unwind: "$supplier_id"},
	{$group: {_id: "$supplier_id", maxPrice: {$max: "$price"}}}
]);

// Get the highest price of a fruit per supplier (that are on sale)
db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", maxPrice: {$max: "$price"}}}
]);

// Get the lowest price of a fruit per supplier
db.fruits.aggregate([
	{$unwind: "$supplier_id"},
	{$group: {_id: "$supplier_id", minPrice: {$min: "$price"}}}
]);

// Get the lowest price of a fruit per supplier (that are on sale)
db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", minPrice: {$min: "$price"}}}
]);





